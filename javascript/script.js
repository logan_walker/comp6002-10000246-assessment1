// Two of each variable required for the imagine page which used 2 modals

// Get the modal
var modal = document.getElementById('myModal');
var modal2 = document.getElementById('myModal2');

// Get the image and insert it inside the modal. Use its alt text as a caption
var img = document.getElementById('myImg');
var img2 = document.getElementById('myImg2');
 
var modalImg = document.getElementById("img01");
var modalImg2 = document.getElementById("img02");

//No caption used so not required
//var captionText = document.getElementById("caption");

// Again, 2 functions required for imagine page
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

img2.onclick = function(){
    modal2.style.display = "block";
    modalImg2.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the span element that closes the modal
//2 span variables required to close each of the modals. One does not close both modals on imagine page
var span = document.getElementsByClassName("close")[0];
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on span (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

span.onclick = function() { 
  modal2.style.display = "none";
}